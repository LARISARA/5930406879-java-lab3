package leelawattananonkul.arisara.lab3;

import java.util.Scanner;
import java.util.Random;

public class DiceGame {
	public static void main (String [] args){
		Scanner Guestnum = new Scanner (System.in);
		Random Computerrolled = new Random();
		int Comrolled = Computerrolled.nextInt(6)+1;
		
		System.out.print("Enter your guest (1-6) : ");
		int Guestnumber = Guestnum.nextInt();
		if(Guestnumber <0 || Guestnumber >6) {
			System.err.println("Incorrect Number. Only 1-6 can be entered.");
			System.exit(0);
		}
		
		System.out.println("You have guested number : " + Guestnumber );
		
		System.out.println("Computer has rolled number : " + Comrolled );
		if (Guestnumber == Comrolled){
			  System.out.println("You win.");
		}
		else if(Guestnumber == Comrolled - 1) {
			System.out.println("You win.");
		}
		else if(Guestnumber == Comrolled + 1) {
			System.out.println("You win.");
		}
		else if(Guestnumber==1 && Comrolled==6) {
			System.out.println("You win.");
		}
		else if(Guestnumber==6 && Comrolled==1) {
			System.out.println("You win.");
		}else {
			System.out.println("Computer wins.");
		}
	}	
}
